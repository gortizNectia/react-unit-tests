import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class ListDinamica extends Component {
  static propTypes = {
    items: PropTypes.array
  }

  constructor(props) {
      super(props);

      this.state = {
          input: '',
          items: []
      }
  }

  render() {
    return (
      <>
        <input 
            type="text" 
            value={this.state.input} 
            onChange={(event)=> {
                this.setState({input: event.target.value}, ()=> {})
            }}
        />
        <button className="btn" onClick={()=> {
          this.setState({items: [...this.state.items, this.state.input], input: ''});
        }}>Button</button>
        <ul>
            {this.state.items.map((item, index)=> <li key={index}>{item}</li>)}
        </ul>
      </>
    )
  }
}
