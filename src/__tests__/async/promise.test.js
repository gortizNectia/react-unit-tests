import { getDataFromApi } from '../../js/promise';

describe('Probando promesas', () => {
    test('Realizando una peticion a una api', done => {
        const api = 'https://rickandmortyapi.com/api/character/';
        console.debug('Antes de getDataFromApi');
        getDataFromApi(api).then(data => {
            expect(data.results.length).toBeGreaterThan(0);
            console.log('Dentro de getDataFromApi then');
            done();
        });
        console.log('Despues de getDataFromApi');
    });

    test('Resuelve un Hola!', () => {
        return expect(Promise.resolve('Hola!')).resolves.toBe('Hola!');
    });

    test('Rechaza con un error', () => {
        return expect(Promise.reject('Errror')).rejects.toBe('Errror');
    });
});