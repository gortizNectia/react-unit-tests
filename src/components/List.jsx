import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class List extends Component {
  static propTypes = {
    items: PropTypes.array
  }

  constructor(props) {
      super(props);

      this.state = {
          input: ''
      }
  }

  render() {
    return (
      <>
        <input 
            type="text" 
            value={this.state.input} 
            onChange={(event)=> {
                this.setState({input: event.target.value}, ()=> {})
                }
            }
        />
        <button className="btn" onClick={()=> console.log(this.state.input)}>Button</button>
        <ul>
            {this.props.items.map((item, index)=> <li key={index}>{item}</li>)}
        </ul>
      </>
    )
  }
}
