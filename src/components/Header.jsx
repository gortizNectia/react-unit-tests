import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Header extends Component {
  static propTypes = {
    prop: PropTypes.object,
    text: PropTypes.string
  }

  render() {
    return (
      <div className="App-header">
        <span>{this.props.text}</span>
      </div>
    )
  }
}
