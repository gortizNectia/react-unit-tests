import React, { Component } from "react";
import Header from "components/Header";
import ListDinamica from "components/ListDinamica";
import "./App.css";
import DatatablePage from "./components/DatatablePage";

//mdboostrap
import "font-awesome/css/font-awesome.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";

//ag grid
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        {/*
          <Header text="Hello world!" />
          <ListDinamica />          
        */}
        <DatatablePage />
      </div>
    );
  }
}

export default App;
