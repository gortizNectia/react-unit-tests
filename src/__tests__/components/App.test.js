import React from "react";
import ReactDOM from "react-dom";
import App from "App";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { shallow } from "enzyme";

import Header from "Header";
import List from "List";
import ListDinamica from "ListDinamica";
import Axios from "axios";

Enzyme.configure({ adapter: new Adapter() });

// it("renders without crashing", () => {
//   const div = document.createElement("div");
//   ReactDOM.render(<App />, div);
//   ReactDOM.unmountComponentAtNode(div);
// });

describe("Test Header Components", () => {

  it("Testing find span component in Header", () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.find("span")).toHaveLength(1);
  });

  it("Testing exist span component in Header", () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.exists("span")).toBe(true);
  });
  
})

describe("Test List Components", () => {
  
  it("Check number elements from List component", () => {
    const wrapper = shallow(<List items={["😁", "😂", "🤣"]} />);
    expect(wrapper.find("li")).toHaveLength(3);
  });

  it("Check number elemets in ListaDinamica when entert a text", () => {
    const wrapper = shallow(<ListDinamica />);

    wrapper.find("input").simulate("change", {
      target: {
        value: "texto de prueba"
      }
    });
    wrapper.find("button").simulate("click");

    wrapper.find("input").simulate("change", {
      target: {
        value: "texto de prueba"
      }
    });
    wrapper.find("button").simulate("click");

    expect(wrapper.find("li")).toHaveLength(2);
  });

  
});

describe("Test api calls with Axios", () => {
  it("Check api return something ", done => {
    const urlApi = "https://jsonplaceholder.typicode.com/todos";

    Axios.get(urlApi).then(data => {
      expect(data).toBeDefined();
      done();
    });
  });
})
