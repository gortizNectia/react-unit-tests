import React from 'react'
import { AgGridReact } from 'ag-grid-react';

import {getApi} from 'services/services'

export default class DatatablePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = { 
      columnDefs: [
        {headerName: 'name', field: 'name'},
        {headerName: 'sition', field: 'sition'},
        {headerName: 'office', field: 'office'},
        {headerName: 'age', field: 'age'},
        {headerName: 'date', field: 'date'},
        {headerName: 'salary', field: 'salary'}
      ],
      rows: []
    }
  } 
  
  componentDidMount() {

    getApi().then((response)=> {

      const rows = response;
      this.setState({
        rows
      });
      
    })
  }

  render() {

    console.log("render");

      return <div 
      className="ag-theme-balham"
      style={{ 
        marginTop: '5px',
        marginLeft: 'auto',
        marginRight: 'auto',
        height: '500px', 
        width: '95%' 
      }} 
    >
        <AgGridReact
            columnDefs={this.state.columnDefs}
            rowData={this.state.rows}>
        </AgGridReact>
    </div>
  }

}
