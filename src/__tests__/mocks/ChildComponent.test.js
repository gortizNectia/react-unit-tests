import React from 'react';
import ChildComponent from 'ChildComponent';
import Enzyme, { mount, shallow} from "enzyme";
import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });

const onSubmitSpy = jest.fn();

const wrapper = shallow(<ChildComponent onSubmit={onSubmitSpy} />);
let container, containerButton;

describe("ChildComponent", () => {
  beforeEach(() => {
    container = wrapper.find("div");
    containerButton = container.find('button');
    onSubmitSpy.mockClear();
  });

  it("should have a <div>", () => {
     expect(container).toHaveLength(1);
  });

  it("should have a <div> with properly className prop", () => {
     expect(container.props().className).toEqual("container");
  });

  it("should have a <Button>", () => {
     expect(containerButton).toHaveLength(1);
  });

  describe("<button> behaviour", () => {
     it("should call onSubmit", () => {
       expect(onSubmitSpy).not.toHaveBeenCalled();
       containerButton.simulate('click');
       expect(onSubmitSpy).toHaveBeenCalled();
       expect(onSubmitSpy).toHaveBeenCalledWith("I’m your son");
     });
  });
});