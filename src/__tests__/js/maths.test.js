import * as math from 'maths.js';

math.restar = jest.fn();

describe('Calculos matematicos', () =>{
    test('Sumar', ()=> {
        expect(math.sumar(1,1)).toBe(2);
    });
    test('Multiplicar ', () => {
        expect(math.multiplicar(2,2)).toBe(4);
    });
    test("Mock para operacion restar", () => {  
        math.restar(2, 1);    
        expect(math.restar).toHaveBeenCalledWith(2, 1);
    });
});