import React from 'react'
import { MDBDataTable } from 'mdbreact';

import {getApi} from 'services/services'

export default class DatatablePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = { 
      data : {
        columns: [
          {
            label: 'Name',
            field: 'name',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Position',
            field: 'position',
            sort: 'asc',
            width: 270
          },
          {
            label: 'Office',
            field: 'office',
            sort: 'asc',
            width: 200
          },
          {
            label: 'Age',
            field: 'age',
            sort: 'asc',
            width: 100
          },
          {
            label: 'Start date',
            field: 'date',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Salary',
            field: 'salary',
            sort: 'asc',
            width: 100
          }
        ],
        rows: []
      }
    }
  } 
  
  componentDidMount() {

    const { data } = this.state;

    getApi().then((response)=> {

      data.rows = response;

      console.log(data)

      this.setState({
        data
      })
    })
    

   /* const response = await fetch('/api/v1/groceries')
    if (response.status >= 400) {
      this.setState({errorStatus: 'Error fetching groceries'});
    } else {
      response.json().then(data => {
        this.setState({groceries: data.groceries})
      });
    }*/
  }

  render() {

    console.log("render")
    console.log(this.state.data);

    return (this.state.data.rows.length === 0 ? <div>Cargando...</div> :
      <MDBDataTable
        striped
        bordered
        hover
        data={this.state.data}
      />
    );
  }

}
